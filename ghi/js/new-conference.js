window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/'
  
    const response = await fetch(url);
  
    if (response.ok) {
      const data = await response.json();
      console.log(data)
    // State option button
      const selectTag = document.getElementById('location');
      for(let location of data.locations){
        // Create an 'option' element
        const option = document.createElement('option');
        // Set the '.value' property of the option element to the
        // state's abbreviation
        option.value = location.id;
        option.innerHTML = location.name;
        selectTag.append(option);
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        console.log(formData);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(conferenceUrl, fetchConfig);
          if(response.ok){
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
          }
    });
  });